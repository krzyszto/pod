#include <stdio.h>

int main(){
	int ileLiczb;
	scanf("%i", &ileLiczb);
	int ileModulo;	//ile modulo nas obchodza liczby
	scanf("%i", &ileModulo);
	int tablicaReszt[ileModulo]; //tablica, gdzie bedziemy trzymac, ile razy dana reszta sie powtorzyla
	for (int i = 0; i < ileModulo; i++){	//ustawianie jej na 0
		tablicaReszt[i] = 0;
	}
	long long int wynik = 1;//wczesniej bylo 1
	int suma = 0;
	for (int i = 0; i < ileLiczb; i++){	//zliczanie, ile dana reszta sie powtorzyla
		int tmp;
		scanf("%i", &tmp);
		tmp %= ileModulo;
		if (tmp == 0) {
			wynik++;
		}
		suma = suma + tmp;
		suma = suma % ileModulo;
		tablicaReszt[suma]++;
	}
	//tablicaReszt[0]++;
	//wynik += tablicaReszt[0] * tablicaReszt[0];
	for (int i = 0; i < ileModulo; i++){
	//	printf("%i ", tablicaReszt[i]);
		wynik += (tablicaReszt[i] * (tablicaReszt[i] - 1)) / 2;
	}
	//printf("\n");
	printf("%lli\n", wynik);
	return 0;
}
